FROM node:16.13.2-alpine3.15 AS build

ENV PUPPETEER_EXECUTABLE_PATH="/usr/bin/chromium-browser" \
    PUPPETEER_SKIP_CHROMIUM_DOWNLOAD="true"

RUN set -xeo pipefail  \
  && apk add --no-cache chromium bash wget gettext \
  && wget -q -O /etc/apk/keys/sgerrand.rsa.pub https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub \
  && wget https://github.com/sgerrand/alpine-pkg-glibc/releases/download/2.33-r0/glibc-2.33-r0.apk \
  && apk add --no-cache glibc-2.33-r0.apk

WORKDIR /usr/src/app

ARG CONTACT_NAME="John Doe"
ARG CONTACT_ADDRESS="Main Street 10"
ARG CONTACT_CITY="12345 Anytown"
ARG CONTACT_COUNTRY="Nowhere"
ARG CONTACT_EMAIL="john.doe@example.org"

COPY package.json package-lock.json ./

RUN set -xeo pipefail  \
  && npm ci

COPY .browserslistrc .eslintrc.json angular.json karma.conf.js tsconfig.app.json tsconfig.json tsconfig.spec.json ./
COPY ./src/ ./src/
COPY ./pacts/ ./pacts/

RUN set -xeo pipefail \
  && envsubst '\$CONTACT_NAME \$CONTACT_ADDRESS \$CONTACT_CITY \$CONTACT_COUNTRY \$CONTACT_EMAIL' < ./src/environments/environment.ts > ./src/environments/environment.prod.ts \
  && $(npm bin)/ng lint \
  && $(npm bin)/ng test --browsers ChromiumHeadlessCI --progress=false --watch=false \
  && $(npm bin)/ng build --configuration production

FROM nginx:1.21.5-alpine

COPY --from=build /usr/src/app/dist/f4ratk-web /usr/share/nginx/html/

COPY docker/sites-enabled/nginx.template.conf /etc/nginx/conf.d/default.conf.template

CMD /bin/sh -c "envsubst '\$PORT \$BACKEND_ADDRESS' < /etc/nginx/conf.d/default.conf.template > /etc/nginx/conf.d/default.conf" && nginx -g 'daemon off;'
