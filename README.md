# f4ratk-web

[![Website: directlink](https://img.shields.io/badge/website-link-informational?cacheSeconds=2592000)](https://f4ratk.web.app)
[![License: AGPL](https://img.shields.io/badge/license-AGPL--3.0--only-informational.svg?cacheSeconds=31536000)](https://spdx.org/licenses/AGPL-3.0-only.html)
[![Build Status: Codeberg](https://ci.codeberg.org/api/badges/toroettg/f4ratk-web/status.svg?cacheSeconds=86400)](https://ci.codeberg.org/toroettg/f4ratk-web)
[![Donate: Liberapay](https://img.shields.io/liberapay/patrons/f4ratk?logo=liberapay?cacheSeconds=2592000)](https://liberapay.com/f4ratk/donate)

The Fama/French Finance Factor Regression Analysis Toolkit for the web.

## About

Web application front end for [f4ratk]. The deployed project is provided
at https://f4ratk.web.app.

## License

This project is licensed under the GNU Affero General Public License version 3 (only). See [LICENSE]
for more information and [COPYING]
for the full license text.

[CONTRIBUTING]: https://codeberg.org/toroettg/f4ratk-web/src/branch/main/CONTRIBUTING.md

[LICENSE]: https://codeberg.org/toroettg/f4ratk-web/src/branch/main/LICENSE

[COPYING]: https://codeberg.org/toroettg/f4ratk-web/src/branch/main/COPYING

[F4RATK]: https://codeberg.org/toroettg/f4ratk
