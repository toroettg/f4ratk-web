import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'app-request-bar',
  templateUrl: './request-bar.component.html',
  styleUrls: ['./request-bar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RequestBarComponent {
  @Input() analyzing!: boolean;

  constructor() {}
}
