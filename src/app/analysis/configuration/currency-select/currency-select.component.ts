import { ChangeDetectionStrategy, Component } from '@angular/core';
import { getCurrencySymbol } from '@angular/common';

type CurrencyType = 'USD' | 'EUR';

interface Currency {
  id: CurrencyType;
}

class SupportedCurrencies {
  static readonly USD: Currency = { id: 'USD' };
  static readonly EUR: Currency = { id: 'EUR' };

  static readonly ALL: Currency[] = [
    SupportedCurrencies.USD,
    SupportedCurrencies.EUR,
  ];
}

@Component({
  selector: 'app-currency-select',
  templateUrl: './currency-select.component.html',
  styleUrls: ['./currency-select.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CurrencySelectComponent {
  default: Currency = SupportedCurrencies.USD;

  availableCurrencies(): Currency[] {
    return SupportedCurrencies.ALL;
  }

  displayFormat(currency: Currency): string {
    return `${getCurrencySymbol(currency.id, 'wide')} (${currency.id})`;
  }

  compareCurrencies(c1: Currency, c2: Currency): boolean {
    return c1.id === c2.id;
  }
}
