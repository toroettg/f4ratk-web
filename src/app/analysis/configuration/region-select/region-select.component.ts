import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

export type RegionType =
  | 'DEVELOPED'
  | 'DEVELOPED-EX-US'
  | 'US'
  | 'EU'
  | 'EMERGING';

export interface Region {
  id: RegionType;
}

export class SupportedRegion implements SupportedRegion {
  static readonly ALL: SupportedRegion[] = [
    SupportedRegion.ofType('DEVELOPED'),
    SupportedRegion.ofType('DEVELOPED-EX-US'),
    SupportedRegion.ofType('US'),
    SupportedRegion.ofType('EU'),
    SupportedRegion.ofType('EMERGING'),
  ];

  private constructor(readonly id: RegionType) {}

  static of(input: string): SupportedRegion | undefined {
    return SupportedRegion.ALL.find((candidate) => candidate.id === input);
  }

  static ofType(type: RegionType): SupportedRegion {
    return new SupportedRegion(type);
  }
}

@Component({
  selector: 'app-region-select',
  templateUrl: './region-select.component.html',
  styleUrls: ['./region-select.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RegionSelectComponent {
  @Input() formGroup!: FormGroup;

  availableRegions(): SupportedRegion[] {
    return SupportedRegion.ALL;
  }
}
