import { FactorResult } from '../ticker/api/ticker-api.service';

type SignificanceLevel = 'LOW' | 'MID' | 'HIGH';

const significanceThreshold = (level: SignificanceLevel): number => {
  if (level === 'LOW') {
    return 0.1;
  } else if (level === 'MID') {
    return 0.05;
  } else if (level === 'HIGH') {
    return 0.01;
  } else {
    throw new RangeError();
  }
};

export const significance = (
  factorResult: FactorResult
): SignificanceLevel | null => {
  const probability = factorResult.probability;

  if (probability <= significanceThreshold('HIGH')) {
    return 'HIGH';
  } else if (probability <= significanceThreshold('MID')) {
    return 'MID';
  } else if (probability <= significanceThreshold('LOW')) {
    return 'LOW';
  }

  return null;
};

export const isSignificant = (factorResult: FactorResult): boolean =>
  significance(factorResult) !== null;
