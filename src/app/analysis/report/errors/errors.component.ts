import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';

interface Error {
  id: ErrorId;
  title: string;
  description: string;
  severity: Severity;
}

export enum ErrorId {
  SHORT_PERIOD,
  LOW_R2,
}

export interface ErrorTemplate {
  id: ErrorId;
  severity: Severity;
}

interface ErrorLabel {
  title: string;
  description: string;
}

export type Severity = 'low-severity' | 'high-severity';

class ErrorFactory {
  public static of(template: ErrorTemplate): Error {
    const label = this.label(template.id);

    return {
      id: template.id,
      title: label.title,
      description: label.description,
      severity: template.severity,
    };
  }

  private static label(error: ErrorId): ErrorLabel {
    switch (error) {
      case ErrorId.SHORT_PERIOD:
        return {
          title: 'Short Period',
          description:
            'The observation period is short and the result is likely to be useless as decisions basis.',
        };
      case ErrorId.LOW_R2:
        return {
          title: 'Low R²',
          description:
            'The result has low explanatory power and is likely to be useless as decisions basis.',
        };
    }
  }
}

@Component({
  selector: 'app-errors',
  templateUrl: './errors.component.html',
  styleUrls: ['./errors.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ErrorsComponent implements OnInit {
  @Input() errorTemplates!: ErrorTemplate[];

  errors: Error[] = [];

  ngOnInit(): void {
    this.errors = this.errorTemplates.map((etl) => ErrorFactory.of(etl));
  }
}
