import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import {
  AnalysisReportResponse,
  FactorResult,
} from '../ticker/api/ticker-api.service';
import { ErrorId, ErrorTemplate, Severity } from './errors/errors.component';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ReportComponent {
  @Input() report!: AnalysisReportResponse;

  graphData(): FactorResult[] {
    return this.report.coefficients.filter(
      (regressand) => regressand.name !== 'Intercept'
    );
  }

  errors(): ErrorTemplate[] {
    return [this.hasLowR2(), this.hasShortPeriod()].filter(
      (value) => value !== undefined
    ) as ErrorTemplate[];
  }

  hasLowR2(): ErrorTemplate | undefined {
    let severity: Severity;

    if (this.report.adjustedRSquared < 0.9) {
      severity = 'high-severity';
    } else if (this.report.adjustedRSquared < 0.95) {
      severity = 'low-severity';
    } else {
      return;
    }

    return {
      id: ErrorId.LOW_R2,
      severity,
    };
  }

  hasShortPeriod(): ErrorTemplate | undefined {
    let severity: Severity;

    if (this.report.observations < 24) {
      severity = 'high-severity';
    } else if (this.report.observations < 60) {
      severity = 'low-severity';
    } else {
      return;
    }

    return {
      id: ErrorId.SHORT_PERIOD,
      severity,
    };
  }
}
