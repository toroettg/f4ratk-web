import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { FactorResult } from '../../ticker/api/ticker-api.service';
import { isSignificant } from '../statistics.service';

interface Legend {
  traceorder: string;
  itemclick: string | boolean;
  itemdoubleclick: string | boolean;
}

interface Axis {
  range?: number[];
  visible?: boolean;
  dtick?: number;
}

interface Layout {
  autosize: boolean;
  title?: string;
  legend: Legend;
  margin: any;
  showlegend?: boolean;
  xaxis?: Axis;
  yaxis?: Axis;
  dragmode: string;
}

interface FactorBox {
  name: string;
  x: number[];
  type: string;
  opacity: number;
}

@Component({
  selector: 'app-factor-chart',
  templateUrl: './factor-chart.component.html',
  styleUrls: ['./factor-chart.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FactorChartComponent implements OnChanges {
  private static readonly LAYOUT: Layout = {
    autosize: true,
    margin: {
      t: 40,
      b: 40,
    },
    legend: {
      traceorder: 'reversed',
      itemclick: false,
      itemdoubleclick: false,
    },
    showlegend: false,
    xaxis: {
      range: [-1.05, 1.05],
      dtick: 0.25,
    },
    dragmode: 'pan',
  };

  private static readonly FACTOR_ORDER: Map<string, number> = new Map<
    string,
    number
  >([
    ['MKT', 1],
    ['SMB', 2],
    ['HML', 3],
    ['RMW', 4],
    ['CMA', 5],
    ['WML', 6],
  ]);

  @Input() results!: FactorResult[];

  data: FactorBox[] = [];

  constructor(private readonly cdr: ChangeDetectorRef) {}

  private static compareFactorBoxes(
    first: FactorBox,
    second: FactorBox
  ): number {
    const firstIndex = FactorChartComponent.FACTOR_ORDER.get(first.name) ?? 0;
    const secondIndex = FactorChartComponent.FACTOR_ORDER.get(second.name) ?? 0;

    if (firstIndex < secondIndex) {
      return 1;
    } else if (firstIndex === secondIndex) {
      return 0;
    } else {
      return -1;
    }
  }

  private static mapFactorResultToBox(factorResult: FactorResult): FactorBox {
    const normalizedWeight =
      factorResult.name === 'MKT'
        ? factorResult.weight - 1
        : factorResult.weight;
    const normalizedConfidenceStart =
      factorResult.name === 'MKT'
        ? factorResult.confidenceStart - 1
        : factorResult.confidenceStart;
    const normalizedConfidenceEnd =
      factorResult.name === 'MKT'
        ? factorResult.confidenceEnd - 1
        : factorResult.confidenceEnd;

    return {
      name: factorResult.name,
      x: [
        normalizedConfidenceStart,
        normalizedWeight - factorResult.standardError,
        normalizedWeight,
        normalizedWeight + factorResult.standardError,
        normalizedConfidenceEnd,
      ],
      type: 'box',
      opacity: FactorChartComponent.opacity(factorResult),
    };
  }

  private static opacity(factorResult: FactorResult): number {
    return isSignificant(factorResult) ? 1 : 0.33;
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.data = this.factorBoxes();
    this.cdr.markForCheck();
  }

  layout(): Layout {
    return FactorChartComponent.LAYOUT;
  }

  factorBoxes(): FactorBox[] {
    return this.results
      .map(FactorChartComponent.mapFactorResultToBox)
      .sort(FactorChartComponent.compareFactorBoxes);
  }
}
