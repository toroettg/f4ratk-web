import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { FactorResult } from '../../ticker/api/ticker-api.service';
import { significance } from '../statistics.service';

@Component({
  selector: 'app-report-table',
  templateUrl: './report-table.component.html',
  styleUrls: ['./report-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ReportTableComponent {
  @Input() results!: FactorResult[];
  @Input() confidenceLevel!: number;

  displayedColumns: string[] = [
    'name',

    'significance',
    'probability',

    'standardError',

    'confidenceStart',
    'weight',
    'confidenceEnd',
  ];

  formattedSignificance(factorResult: FactorResult): '*' | '**' | '***' | null {
    const level = significance(factorResult);
    if (level === 'HIGH') {
      return '***';
    } else if (level === 'MID') {
      return '**';
    } else if (level === 'LOW') {
      return '*';
    } else {
      return null;
    }
  }
}
