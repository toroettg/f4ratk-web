import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AnalysisReportResponse } from '../../ticker/api/ticker-api.service';
import { HttpClient } from '@angular/common/http';
import { FileAnalysisConfiguration } from '../file-analysis-configuration/file-analysis-configuration.component';

export interface Quote {
  date: string;
  value: number;
}

interface Configuration {
  region: string;
}

@Injectable({
  providedIn: 'root',
})
export class FileApiService {
  private static readonly RESOURCE = '/api/v0/files';

  constructor(private readonly http: HttpClient) {}

  fetchAnalysisReport(
    configuration: FileAnalysisConfiguration
  ): Observable<AnalysisReportResponse> {
    const body: Configuration = { region: configuration.region.id };

    const formData = new FormData();

    formData.append('file', configuration.file, configuration.file.name);
    formData.set('config', JSON.stringify(body));

    return this.http.post<AnalysisReportResponse>(
      `${FileApiService.RESOURCE}`,
      formData,
      { headers: { timeout: `${90000}` } }
    );
  }
}
