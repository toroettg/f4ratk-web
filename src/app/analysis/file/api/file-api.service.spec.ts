import { TestBed } from '@angular/core/testing';

import { FileApiService } from './file-api.service';
import { HttpClientModule } from '@angular/common/http';

describe('FileApiService', () => {
  let service: FileApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
    });
    service = TestBed.inject(FileApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
