import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  forwardRef,
  ViewChild,
} from '@angular/core';
import { MatExpansionPanel } from '@angular/material/expansion';
import { Papa, ParseResult } from 'ngx-papaparse';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Quote } from '../../api/file-api.service';

@Component({
  selector: 'app-file-input-selection',
  templateUrl: './file-select.component.html',
  styleUrls: ['./file-select.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => FileSelectComponent),
    },
  ],
})
export class FileSelectComponent implements ControlValueAccessor {
  @ViewChild('fileUpload') fileUpload!: ElementRef;
  @ViewChild(MatExpansionPanel) panel!: MatExpansionPanel;

  file: File | undefined;
  quotes: Quote[] | undefined;

  constructor(
    private readonly papa: Papa,
    private readonly cdr: ChangeDetectorRef
  ) {}

  private static parseFileContent(rows: Array<[string, number]>): Quote[] {
    return rows.map(FileSelectComponent.parseRow);
  }

  private static parseRow(row: [string, number]): Quote {
    return {
      date: row[0],
      value: row[1],
    };
  }

  onChange: (selection: File | undefined) => void = () => {};

  onTouched: () => void = () => {};

  /**
   *  stopPropagation has no effect if clicked on area near boarder
   */
  ensureNotOpenWhenEmpty(): void {
    if (!this.quotes) {
      this.panel.close();
    }
  }

  onPanelClicked(event: Event): void {
    if (!this.quotes) {
      event.stopPropagation();
      this.fileUpload.nativeElement.click();
    }
  }

  onPanelIconClicked(event: Event): void {
    if (this.quotes) {
      event.stopPropagation();
      this.removeFileContent();
    }
  }

  onFileSelected(event: Event): void {
    const file: File | null | undefined = (
      event.currentTarget as HTMLInputElement
    ).files?.item(0);

    if (!file) {
      return;
    }

    this.papa.parse(file, {
      comments: '#',
      skipEmptyLines: true,
      dynamicTyping: true,
      complete: (results, parsedFile) => this.parseFile(results, parsedFile),
    });
  }

  removeFileContent(): void {
    this.file = undefined;
    this.quotes = undefined;

    this.panel.close();

    this.onChange(undefined);

    this.cdr.markForCheck();
  }

  registerOnChange(callback: (selection: File | undefined) => void): void {
    this.onChange = callback;
  }

  registerOnTouched(callback: () => void): void {
    this.onTouched = callback;
  }

  writeValue(selection: File | undefined): void {
    return;
  }

  private parseFile(result: ParseResult, file: File | undefined): void {
    if (file === undefined) {
      return;
    }

    const quotes = FileSelectComponent.parseFileContent(result.data);

    this.setFileContent(file, quotes);
  }

  private setFileContent(file: File, quotes: Quote[]): void {
    this.file = file;
    this.quotes = quotes;

    this.onChange(file);

    this.cdr.markForCheck();
  }
}
