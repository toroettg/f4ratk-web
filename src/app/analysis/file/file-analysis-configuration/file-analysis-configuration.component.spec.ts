import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FileAnalysisConfigurationComponent } from './file-analysis-configuration.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FileSelectComponent } from './file-select/file-select.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSelectModule } from '@angular/material/select';

describe('FileAnalysisConfigurationComponent', () => {
  let component: FileAnalysisConfigurationComponent;
  let fixture: ComponentFixture<FileAnalysisConfigurationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, BrowserAnimationsModule, MatSelectModule],
      providers: [],
      declarations: [FileAnalysisConfigurationComponent, FileSelectComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FileAnalysisConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
