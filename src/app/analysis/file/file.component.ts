import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
} from '@angular/core';
import { AnalysisReportResponse } from '../ticker/api/ticker-api.service';
import { FileAnalysisConfiguration } from './file-analysis-configuration/file-analysis-configuration.component';
import { FileApiService } from './api/file-api.service';

@Component({
  selector: 'app-file',
  templateUrl: './file.component.html',
  styleUrls: ['./file.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FileComponent {
  analysisReport: AnalysisReportResponse | undefined;

  isAnalyzing = false;

  constructor(
    private readonly fileApiService: FileApiService,
    private readonly cdr: ChangeDetectorRef
  ) {}

  analyze(config: FileAnalysisConfiguration): void {
    this.isAnalyzing = true;
    this.fileApiService.fetchAnalysisReport(config).subscribe(
      (response) => this.displayReport(response),
      () => this.clearReport()
    );
  }

  private displayReport(report: AnalysisReportResponse): void {
    this.analysisReport = report;
    this.isAnalyzing = false;
    this.cdr.markForCheck();
  }

  private clearReport(): void {
    this.analysisReport = undefined;
    this.isAnalyzing = false;
    this.cdr.markForCheck();
  }
}
