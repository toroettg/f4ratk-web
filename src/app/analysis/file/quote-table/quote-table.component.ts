import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Quote } from '../api/file-api.service';

@Component({
  selector: 'app-quote-table',
  templateUrl: './quote-table.component.html',
  styleUrls: ['./quote-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class QuoteTableComponent {
  @Input() quotes!: Quote[];

  readonly displayedColumns: string[] = ['date', 'value'];
}
