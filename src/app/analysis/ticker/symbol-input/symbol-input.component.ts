import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
} from '@angular/core';
import { AbstractControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-symbol-input',
  templateUrl: './symbol-input.component.html',
  styleUrls: ['./symbol-input.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SymbolInputComponent {
  @Input() formGroup!: FormGroup;
  @Input() formName!: string;

  constructor(private readonly cdr: ChangeDetectorRef) {}

  severity(): string {
    return this.hasError() ? 'high-severity' : 'low-severity';
  }

  hasError(): boolean {
    return this.symbolForm().hasError('noData') ?? false;
  }

  errorDescription(): string {
    const hasExchangeSeparator = this.symbol().includes('.');

    return hasExchangeSeparator
      ? 'No data available. <em>JPGL.L</em>, <em>USSC.L</em> or <em>QMOM</em> are other symbols to try out.'
      : 'No data available. Specify the stock exchange, i.e., use <em>JPGL.L</em> instead of <em>JPGL</em> where applicable.';
  }

  onChange(): void {
    this.symbolForm().patchValue(this.symbolForm().value.toUpperCase());
  }

  setSymbol(symbol: string): void {
    this.symbolForm()?.setValue(symbol);
  }

  symbol(): string {
    return this.symbolForm().value;
  }

  markNoDataError(): void {
    this.symbolForm().setErrors({
      noData: true,
    });

    this.symbolForm().markAsTouched();

    this.cdr.markForCheck();
  }

  private symbolForm(): AbstractControl {
    return this.formGroup.get(this.formName) as AbstractControl;
  }
}
