import { TestBed } from '@angular/core/testing';

import { TickerApiService } from './ticker-api.service';
import { Matchers, PactWeb } from '@pact-foundation/pact-web';
import { HttpClientModule } from '@angular/common/http';
import { SupportedRegion } from '../../configuration/region-select/region-select.component';

describe('TickerApiService', () => {
  let provider: PactWeb;

  let service: TickerApiService;

  beforeAll(() => {
    provider = new PactWeb();

    provider.removeInteractions();
  });

  afterAll((done) => {
    provider.finalize().then(done, done.fail);
  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [TickerApiService],
    });
    service = TestBed.inject(TickerApiService);
  });

  afterEach((done) => {
    provider.verify().then(done, (e) => done.fail(e));
  });

  describe('fetchTicker()', () => {
    beforeAll((done) => {
      provider
        .addInteraction({
          state: `provider can not lookup data for ticker symbol`,
          uponReceiving: 'a request to GET a analysis report',
          withRequest: {
            method: 'GET',
            path: '/v0/tickers/missing;region=US',
            headers: {
              'Content-Type': 'application/json',
            },
          },
          willRespondWith: {
            status: 404,
            headers: {
              'Content-Type': 'application/json',
            },
          },
        })
        .then(done, (error) => done.fail(error));
    });

    it('should fail analysis report', (done) => {
      service
        .fetchAnalysisReport('missing', SupportedRegion.ofType('US'))
        .subscribe(
          (response) => done.fail('Should not respond without error'),
          (error) => {
            done();
          }
        );
    });
  });

  describe('fetchTickerAnalysis()', () => {
    beforeAll((done) => {
      provider
        .addInteraction({
          state: `provider accepts a new ticker symbol`,
          uponReceiving: 'a request to GET a analysis report',
          withRequest: {
            method: 'GET',
            path: Matchers.term({
              generate: '/v0/tickers/AVDV;region=DEVELOPED-EX-US',
              matcher: '^/v0/tickers/(.*?);region=([A-Z-]+)$',
            }),
            headers: {
              'Content-Type': 'application/json',
            },
          },
          willRespondWith: {
            status: 200,
            body: Matchers.somethingLike({
              coefficients: [
                {
                  name: 'Intercept',
                  weight: 0.0545,
                  standardError: 0.288,
                  probability: 0.189,
                  confidenceStart: -0.408,
                  confidenceEnd: 0.0,
                },
                {
                  name: 'MKT',
                  weight: 1.0514,
                  standardError: 0.015,
                  probability: 0.0,
                  confidenceStart: 1.022,
                  confidenceEnd: 1.082,
                },
                {
                  name: 'SMB',
                  weight: 0.7467,
                  standardError: 0.034,
                  probability: 0.007,
                  confidenceStart: 0.679,
                  confidenceEnd: 0.813,
                },
                {
                  name: 'HML',
                  weight: 0.3088,
                  standardError: 0.044,
                  probability: 0.035,
                  confidenceStart: 0.223,
                  confidenceEnd: 0.389,
                },
                {
                  name: 'RMW',
                  weight: 0.2834,
                  standardError: 0.048,
                  probability: 0.0095,
                  confidenceStart: 0.189,
                  confidenceEnd: 0.37,
                },
                {
                  name: 'CMA',
                  weight: 0.0391,
                  standardError: 0.048,
                  probability: 0.416,
                  confidenceStart: -0.048,
                  confidenceEnd: 0.134,
                },
                {
                  name: 'WML',
                  weight: -0.1988,
                  standardError: 0.03,
                  probability: 0.075,
                  confidenceStart: -0.258,
                  confidenceEnd: -0.139,
                },
              ],
              confidenceLevel: 0.95,
              adjustedRSquared: 0.9698,
              observations: 314,
              excessReturn: 0.425,
            }),
            headers: {
              'Content-Type': 'application/json',
            },
          },
        })
        .then(done, (error) => done.fail(error));
    });

    it('should fetch analysis report', (done) => {
      service
        .fetchAnalysisReport('A12ATF.DE', SupportedRegion.ofType('EMERGING'))
        .subscribe(
          (response) => {
            expect(response.confidenceLevel).toEqual(0.95);
            expect(response.observations).toEqual(314);
            expect(response.excessReturn).toEqual(0.425);
            expect(response.adjustedRSquared).toEqual(0.9698);

            expect(
              response.coefficients.find((value) => value.name === 'MKT')
                ?.weight
            ).toEqual(1.0514);
            expect(
              response.coefficients.find((value) => value.name === 'MKT')
                ?.standardError
            ).toEqual(0.015);
            expect(
              response.coefficients.find((value) => value.name === 'MKT')
                ?.probability
            ).toEqual(0.0);

            expect(
              response.coefficients.find((value) => value.name === 'SMB')
                ?.weight
            ).toEqual(0.7467);
            expect(
              response.coefficients.find((value) => value.name === 'HML')
                ?.weight
            ).toEqual(0.3088);
            expect(
              response.coefficients.find((value) => value.name === 'RMW')
                ?.weight
            ).toEqual(0.2834);
            expect(
              response.coefficients.find((value) => value.name === 'CMA')
                ?.weight
            ).toEqual(0.0391);
            expect(
              response.coefficients.find((value) => value.name === 'CMA')
                ?.probability
            ).toEqual(0.416);
            expect(
              response.coefficients.find((value) => value.name === 'WML')
                ?.weight
            ).toEqual(-0.1988);

            expect(
              response.coefficients.find((value) => value.name === 'Intercept')
                ?.weight
            ).toEqual(0.0545);

            done();
          },
          (error) => done.fail(error)
        );
    });
  });
});
