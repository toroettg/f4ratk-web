import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Region } from '../../configuration/region-select/region-select.component';

export interface FactorResult {
  readonly name: string;
  readonly weight: number;
  readonly standardError: number;
  readonly probability: number;
  readonly confidenceStart: number;
  readonly confidenceEnd: number;
}

export interface AnalysisReportResponse {
  coefficients: FactorResult[];

  confidenceLevel: number;
  adjustedRSquared: number;
  observations: number;
  excessReturn: number;
}

@Injectable({
  providedIn: 'root',
})
export class TickerApiService {
  private static readonly RESOURCE = '/api/v0/tickers';

  constructor(private readonly http: HttpClient) {}

  fetchAnalysisReport(
    symbol: string,
    region: Region
  ): Observable<AnalysisReportResponse> {
    return this.http.get<AnalysisReportResponse>(
      `${TickerApiService.RESOURCE}/${symbol};region=${region.id}`,
      { headers: { 'Content-Type': 'application/json', timeout: `${90000}` } }
    );
  }
}
