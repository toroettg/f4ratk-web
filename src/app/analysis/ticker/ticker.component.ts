import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ViewChild,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  AnalysisReportResponse,
  TickerApiService,
} from './api/ticker-api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SupportedRegion } from '../configuration/region-select/region-select.component';
import { HttpErrorResponse } from '@angular/common/http';
import { SymbolInputComponent } from './symbol-input/symbol-input.component';

interface Config {
  symbol: string | undefined;
  region: SupportedRegion | undefined;
}

@Component({
  selector: 'app-ticker',
  templateUrl: './ticker.component.html',
  styleUrls: ['./ticker.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TickerComponent implements AfterViewInit {
  @ViewChild(SymbolInputComponent) symbolInput!: SymbolInputComponent;

  analysisReport: AnalysisReportResponse | undefined;

  analysisForm: FormGroup = this.formBuilder.group({
    symbol: [undefined, Validators.required],
    region: [undefined, Validators.required],
  });

  isAnalyzing = false;

  constructor(
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly tickerApiService: TickerApiService,
    private readonly formBuilder: FormBuilder,
    private readonly cdr: ChangeDetectorRef
  ) {}

  private static parseRouteParamToConfig(param: string | undefined): Config {
    const segments = (param ?? '').split(':');

    return {
      symbol: segments[0],
      region: SupportedRegion.of(segments[1]),
    };
  }

  ngAfterViewInit(): void {
    this.route.params.subscribe((params) => {
      const config = TickerComponent.parseRouteParamToConfig(params.config);

      if (config.symbol) {
        this.symbolInput.setSymbol(config.symbol);
      }

      if (config.region) {
        this.setRegion(config.region);
      }

      if (this.analysisForm.valid) {
        this.analyzeInputs();
      }
    });
  }

  onSubmit(): void {
    this.router.navigate(['/tickers', this.configAsRouteParam()]);
  }

  private configAsRouteParam(): string {
    return `${this.symbolInput.symbol()}:${this.region().id}`;
  }

  private analyzeInputs(): void {
    this.isAnalyzing = true;
    this.tickerApiService
      .fetchAnalysisReport(this.symbolInput.symbol(), this.region())
      .subscribe(
        (response) => this.setReport(response),
        (error) => this.handleAnalysisError(error)
      );
  }

  private region(): SupportedRegion {
    return this.analysisForm.get('region')?.value;
  }

  private setRegion(region: SupportedRegion): void {
    this.analysisForm.get('region')?.setValue(region);
  }

  private setReport(analysisReport: AnalysisReportResponse): void {
    this.analysisReport = analysisReport;
    this.isAnalyzing = false;
    this.cdr.markForCheck();
  }

  private clearReport(): void {
    this.analysisReport = undefined;
    this.isAnalyzing = false;
    this.cdr.markForCheck();
  }

  private handleAnalysisError(error: HttpErrorResponse): void {
    if (error.status === 404) {
      this.symbolInput.markNoDataError();
    }

    this.clearReport();
  }
}
