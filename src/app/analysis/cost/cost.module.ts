import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CostComponent } from './cost/cost.component';
import { MatTableModule } from '@angular/material/table';
import { MatDividerModule } from '@angular/material/divider';
import { ReportStatementsFormComponent } from './cost/report-statements-form/report-statements-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { CostBreakdownTableComponent } from './cost/cost-breakdown-table/cost-breakdown-table.component';
import { SharedModule } from '../shared/shared.module';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  declarations: [
    CostComponent,
    ReportStatementsFormComponent,
    ReportStatementsFormComponent,
    CostBreakdownTableComponent,
  ],
  imports: [
    CommonModule,
    MatTableModule,
    MatDividerModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    FlexLayoutModule,
    MatProgressBarModule,
    SharedModule,
    MatCardModule,
    MatIconModule,
  ],
})
export class CostModule {}
