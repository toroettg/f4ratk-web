import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { CostBreakdownResponse } from '../api/cost-api.service';

export interface CostBreakdownItem {
  title: string;
  amount: string;
}

@Component({
  selector: 'app-cost-breakdown-table',
  templateUrl: './cost-breakdown-table.component.html',
  styleUrls: ['./cost-breakdown-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DecimalPipe],
})
export class CostBreakdownTableComponent {
  @Input() costBreakdown!: CostBreakdownResponse;

  readonly displayedColumns: string[] = ['title', 'amount'];

  constructor(private readonly decimalPipe: DecimalPipe) {}

  private static displayTitle(title: string, hasAmount: boolean): string {
    const marker = hasAmount ? ' (not applicable)' : '';

    return `${title}${marker}`;
  }

  items(): CostBreakdownItem[] {
    return [
      this.displayItem(
        'Total Expense',
        this.costBreakdown.increases.totalExpenseRatio
      ),
      this.displayItem(
        'Withholding Tax',
        this.costBreakdown.increases.withholdingTaxRatio
      ),
      this.displayItem(
        'Capital Gains Tax',
        this.costBreakdown.increases.capitalGainsTaxRatio
      ),
      this.displayItem(
        'Transaction Costs',
        this.costBreakdown.increases.transactionCostsRatio
      ),
      this.displayItem(
        'Securities Lending Income',
        this.costBreakdown.decreases.securitiesLendingRatio,
        true
      ),
    ];
  }

  hasAmount(item: CostBreakdownItem): boolean {
    return !item.amount;
  }

  displayTotal(): string {
    return this.displayAmount(this.costBreakdown.totalCostsRatio);
  }

  private displayItem(
    title: string,
    amount?: number,
    decreases: boolean = false
  ): CostBreakdownItem {
    return {
      title: CostBreakdownTableComponent.displayTitle(title, !amount),
      amount: this.displayAmount(amount && decreases ? -amount : amount),
    };
  }

  private displayAmount(amount?: number): string {
    const value = this.decimalPipe.transform(amount, '.3');

    return value ? `${value}%` : '';
  }
}
