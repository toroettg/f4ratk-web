import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

export interface CostBreakdownRequest {
  totalExpenseRatio: number;
  expenses: number;
  transactionCosts?: number;
  withholdingTax?: number;
  capitalGainsTax?: number;
  securitiesLendingIncome?: number;
}

export interface CostBreakdownResponse {
  increases: {
    totalExpenseRatio: number;
    withholdingTaxRatio?: number;
    capitalGainsTaxRatio?: number;
    transactionCostsRatio?: number;
  };
  decreases: { securitiesLendingRatio?: number };
  totalCostsRatio: number;
}

@Injectable({
  providedIn: 'root',
})
export class CostApiService {
  private static readonly RESOURCE = '/api/v0/costs';

  constructor(private readonly http: HttpClient) {}

  fetchCostBreakdown(
    costBreakdownRequest: CostBreakdownRequest
  ): Observable<CostBreakdownResponse> {
    return this.http.post<CostBreakdownResponse>(
      CostApiService.RESOURCE,
      costBreakdownRequest,
      {
        headers: { timeout: `${90000}` },
      }
    );
  }
}
