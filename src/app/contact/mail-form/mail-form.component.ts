import { ChangeDetectionStrategy, Component } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MailApiService, MailRequest } from '../api/mail-api.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-mail-form',
  templateUrl: './mail-form.component.html',
  styleUrls: ['./mail-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MailFormComponent {
  mailForm: FormGroup = this.formBuilder.group({
    name: [undefined],
    additionalName: [undefined],
    email: [undefined, Validators.email],
    address: [undefined],
    content: [undefined],
  });

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly mailApiService: MailApiService,
    private readonly snackBar: MatSnackBar
  ) {}

  onSubmit(): void {
    this.mailApiService.relayMail(this.message()).subscribe(
      (response) => this.notifySent(),
      (error) => this.notifyError()
    );
  }

  emailControl(): AbstractControl {
    return this.mailForm.get('email') as AbstractControl;
  }

  emailControlSeverity(): string {
    return this.emailControl().valid ? 'low-severity' : 'high-severity';
  }

  private message(): MailRequest {
    return {
      name: this.nameControl().value || undefined,
      additionalName: this.additionalNameControl().value || undefined,
      contact: this.emailControl().value || undefined,
      address: this.addressControl().value || undefined,
      content: this.contentControl().value || undefined,
    };
  }

  private notifySent(): void {
    this.snackBar.open('Your message has been sent, thank you!', undefined, {
      duration: 10000,
    });
    this.mailForm.reset();
  }

  private notifyError(): void {
    this.snackBar.open(
      'Unable to send your message, please try again or fall back to other means of contact.',
      undefined,
      { duration: 15000 }
    );
  }

  private nameControl(): AbstractControl {
    return this.mailForm.get('name') as AbstractControl;
  }

  private additionalNameControl(): AbstractControl {
    return this.mailForm.get('additionalName') as AbstractControl;
  }

  private addressControl(): AbstractControl {
    return this.mailForm.get('address') as AbstractControl;
  }

  private contentControl(): AbstractControl {
    return this.mailForm.get('content') as AbstractControl;
  }
}
