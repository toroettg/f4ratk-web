import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

export interface MailRequest {
  name: string | undefined;
  contact: string | undefined;
  content: string | undefined;
  address: string | undefined;
  additionalName: string | undefined;
}

@Injectable({
  providedIn: 'root',
})
export class MailApiService {
  private static readonly RESOURCE = '/api/v0/mails';

  constructor(private readonly http: HttpClient) {}

  relayMail(mail: MailRequest): Observable<void> {
    return this.http.post<void>(MailApiService.RESOURCE, mail, {
      headers: { timeout: `${90000}` },
    });
  }
}
