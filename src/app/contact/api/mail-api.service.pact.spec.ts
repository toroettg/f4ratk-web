import { TestBed } from '@angular/core/testing';
import { Matchers, PactWeb } from '@pact-foundation/pact-web';
import { HttpClientModule } from '@angular/common/http';
import { MailApiService } from './mail-api.service';

describe('MailApiService', () => {
  let provider: PactWeb;

  let service: MailApiService;

  beforeAll(() => {
    provider = new PactWeb();

    provider.removeInteractions();
  });

  afterAll((done) => {
    provider.finalize().then(done, done.fail);
  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [MailApiService],
    });
    service = TestBed.inject(MailApiService);
  });

  afterEach((done) => {
    provider.verify().then(done, (e) => done.fail(e));
  });

  describe('relayMailMessage()', () => {
    beforeAll((done) => {
      provider
        .addInteraction({
          state: `provider accepts a new mail request`,
          uponReceiving: 'a request to POST a mail message',
          withRequest: {
            method: 'POST',
            path: '/v0/mails',
            headers: {
              'Content-Type': Matchers.string('application/json'),
            },
          },
          willRespondWith: {
            status: 204,
            headers: {
              'Content-Type': 'application/json',
            },
          },
        })
        .then(done, (error) => done.fail(error));
    });

    it('should relay mail message request', (done) => {
      service
        .relayMail({
          name: 'John Doe',
          additionalName: 'Some Strange Input',
          contact: 'john.doe@example.org',
          address: 'Fakeavenue',
          content: 'Some Feedback',
        })
        .subscribe(
          (response) => {
            done();
          },
          (error) => done.fail(error)
        );
    });
  });
});
