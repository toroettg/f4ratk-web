import { ChangeDetectionStrategy, Component } from '@angular/core';
import { environment } from '../../environments/environment';
import { Router } from '@angular/router';
import { PlatformLocation } from '@angular/common';

@Component({
  selector: 'app-privacy-policy',
  templateUrl: './privacy-policy.component.html',
  styleUrls: ['./privacy-policy.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PrivacyPolicyComponent {
  readonly name: string = environment.contact.name;
  readonly address: string = environment.contact.address;
  readonly city: string = environment.contact.city;
  readonly country: string = environment.contact.country;
  readonly email: string = environment.contact.email;

  readonly legalNoticeUrl: string;

  constructor(
    private readonly platformLocation: PlatformLocation,
    private readonly router: Router
  ) {
    this.legalNoticeUrl =
      (this.platformLocation as any).location.origin +
      router.createUrlTree(['/legal-notice']);
  }
}
